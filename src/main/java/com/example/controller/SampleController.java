package com.example.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.Model.UserAccount;
import com.example.bestexceptionhandler.ExceptionThrower;

@Controller
public class SampleController extends SpringBootServletInitializer {

	ExceptionThrower et = new ExceptionThrower();
	
	@Autowired
	UserAccount userAccount; 
	
    @RequestMapping("/exception")
    public String exception() throws Exception {
        et.throwGeneralException();
    	//et.throwNullPointerException();
		return null;
    }

    @RequestMapping("/custom-exception")
    public String customException() throws Exception {
    	et.throwCustomException();
		return null;
    }
    
    @RequestMapping("/nullpoint-exception")
    public String nullpoint() throws Exception {   	
    	et.throwNullPointerException();
		return null;
    }
}	