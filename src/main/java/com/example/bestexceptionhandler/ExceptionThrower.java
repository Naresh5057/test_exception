package com.example.bestexceptionhandler;

public class ExceptionThrower {

	public void throwGeneralException() throws Exception{
		Exception e = new Exception("Error from General Exception");
		throw e;
	}
	
	public void throwCustomException() throws CustomException {
		
		CustomException e = new CustomException();
		e.setCode(1);
		e.setDescription("First Name not provided");
		throw e;
	}
	
	public void throwNullPointerException() {
		int i=0,j;
		j=i/0; 
	}
}
