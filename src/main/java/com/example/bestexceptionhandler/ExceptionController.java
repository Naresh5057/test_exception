package com.example.bestexceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import scala.annotation.meta.setter;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionResponse>generalexception(Exception ex) throws Exception{
		
		ExceptionResponse er = new ExceptionResponse();
		er.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		er.setMessage(ex.getMessage());
		return new ResponseEntity<ExceptionResponse>(er,HttpStatus.INTERNAL_SERVER_ERROR);	
	}
	
	@ExceptionHandler(CustomException.class)
	public ResponseEntity<ExceptionResponse>Specialexception(CustomException ex) throws Exception{
		
		ExceptionResponse er = new ExceptionResponse();
		er.setCode(HttpStatus.BAD_REQUEST.value());
		er.setMessage(ex.getDescription());
		return new ResponseEntity<ExceptionResponse>(er,HttpStatus.BAD_REQUEST);	
	}
}
